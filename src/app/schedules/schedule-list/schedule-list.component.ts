import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit {
  schedules: {name: string, caloriesPerDay: number}[] = [
    {name: 'Bulking', caloriesPerDay: 3600},
    {name: 'Cutting', caloriesPerDay: 2600},
    {name: 'Lean Muscle Building', caloriesPerDay: 3000}
  ];

  constructor() { }

  ngOnInit() {
  }

}
