import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-schedule-summary',
  templateUrl: './schedule-summary.component.html',
  styleUrls: ['./schedule-summary.component.css']
})
export class ScheduleSummaryComponent implements OnInit {
  @Input() name: string;
  @Input() caloriesPerDay: number;

  constructor() { }

  ngOnInit() {
  }

}
