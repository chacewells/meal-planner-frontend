import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ingredient-summary',
  templateUrl: './ingredient-summary.component.html',
  styleUrls: ['./ingredient-summary.component.css']
})
export class IngredientSummaryComponent implements OnInit {
  @Input() name: string;
  @Input() portion: number;
  @Input() portionUom: string;

  constructor() { }

  ngOnInit() {
  }

}
