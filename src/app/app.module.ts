import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecipeSummaryComponent } from './recipes/recipe-list/recipe-summary/recipe-summary.component';
import { RecipeDetailComponent }  from './recipes/recipe-detail/recipe-detail.component';
import { IngredientSummaryComponent } from './ingredient-summary/ingredient-summary.component';
import { RecipesComponent } from './recipes/recipes.component';
import { MealsComponent } from './meals/meals.component';
import { SchedulesComponent } from './schedules/schedules.component';
import { HeaderComponent } from './header/header.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { MealListComponent } from './meals/meal-list/meal-list.component';
import { MealSummaryComponent } from './meals/meal-list/meal-summary/meal-summary.component';
import { ScheduleListComponent } from './schedules/schedule-list/schedule-list.component';
import { ScheduleSummaryComponent } from './schedules/schedule-list/schedule-summary/schedule-summary.component';

@NgModule({
  declarations: [
    AppComponent,
    RecipeSummaryComponent,
    RecipeDetailComponent,
    IngredientSummaryComponent,
    RecipesComponent,
    MealsComponent,
    SchedulesComponent,
    HeaderComponent,
    RecipeListComponent,
    MealListComponent,
    MealSummaryComponent,
    ScheduleListComponent,
    ScheduleSummaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
