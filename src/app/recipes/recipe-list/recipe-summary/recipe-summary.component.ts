import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-recipe-summary',
  templateUrl: './recipe-summary.component.html',
  styleUrls: ['./recipe-summary.component.css']
})
export class RecipeSummaryComponent implements OnInit {
  @Input() name: string;
  @Input() calories: number;

  constructor() { }

  ngOnInit() {
  }

}
