import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes = [
    {name: 'Hamburger', calories: 398},
    {name: 'Tuna Sandwich', calories: 200},
    {name: 'Daal Curry', calories: 300},
    {name: 'Sayeda is cute', calories: 1000}
  ];

  constructor() { }

  ngOnInit() {
  }

}
