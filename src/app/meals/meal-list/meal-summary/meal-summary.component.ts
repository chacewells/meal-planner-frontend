import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-meal-summary',
  templateUrl: './meal-summary.component.html',
  styleUrls: ['./meal-summary.component.css']
})
export class MealSummaryComponent implements OnInit {
  @Input() name: string;
  @Input() calories: number;

  constructor() { }

  ngOnInit() {
  }

}
