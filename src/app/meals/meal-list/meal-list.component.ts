import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meal-list',
  templateUrl: './meal-list.component.html',
  styleUrls: ['./meal-list.component.css']
})
export class MealListComponent implements OnInit {
  meals: {name: string, calories: number}[] = [
    {name: 'Hamburger Meal', calories: 700},
    {name: 'Butter Chicken with Rice', calories: 500},
    {name: 'Tuna Sandwich and Veggies', calories: 600}
  ];

  constructor() { }

  ngOnInit() {
  }

}
